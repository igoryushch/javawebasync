/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.web.servlets;

import java.io.IOException;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author CodeFire
 */
@WebServlet("/simple")
public class SimpleServlet extends HttpServlet {

    // TODO: set to 'true' for emulate long time operations
    private static final boolean debug = true;
    private static final int timeout = 5000;

    private static final Logger LOGGER = Logger.getLogger(SimpleServlet.class.getName());

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            long start = System.currentTimeMillis();
            if (debug) {
                Thread.sleep(timeout);
            }
            long duration = System.currentTimeMillis() - start;
            response.setContentType("text/plain");
            response.getWriter().printf("Thread %s completed the task in %d ms.", Thread.currentThread().getName(), duration);
        } catch (InterruptedException | IOException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("username");
        if (name == null || name.trim().isEmpty()) {
            request.setAttribute("error", "Input correct name!");
        } else {
            request.setAttribute("message", String.format("Hello, %s!", name));
        }
        request.getRequestDispatcher("simple.jsp").forward(request, response);
    }
}
